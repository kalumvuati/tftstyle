//TODO Faire sortir les selecteurs(classes/ids) hors de fonctions.
//TODO chaque fonction doit être applée en passant le selecteur en argument.
class TFT {

    constructor() {
        /*SCROLL EFFECT*/
        /*---------------------------VARIABLES/CONSTANTES------------------------*/
        /**
         * @var constante pour la séconde par de navégation dans la page CV
         */
        this.cv_nav_bar = document.querySelector("#cv-bar");
        /*SCROLL SUR LES LIENS INTERNES*/
        this.scroll_animation = $('.scroll-animate');
        /**
         * @brief elle est true pour afficher sinon, pas afficher le side bar
         */
        this.aside_profile_checker = true;

        this.carousel_iterator = 0;
        /* FONCTIONS CALLING*/
        this.dropdown();
        this.linkEvent();
        this.alert();
        this.aside('.aside-list-static');
        this.aside_profile_close(".profile-close");
        this.activateScroll('.footer-go-up');
        this.activate_go_up(".footer-go-up");
        this.activate_link_scroll();
        this.formular_checker(".form-group");
        this.card_show_back_items();
        this.form_input_animate();
        this.slider(".slider");
        this.carousel(".carousel");
        this.accordeon(".accordeon-title");
        this.process(".process");
        this.pulse(".pulse");

    }

    /**
     * fonction à améliorer à l'avenir.
     * elle est inutile pour l'instant
     * @param {*} selector_p 
     */
    pulse(selector_p) {
        let pulse_v = document.querySelector(selector_p);
        const colors = ['#67C23A', '#E6A23C', '#F56C6C', '#33B5E5', '#4285F4', '#AA66CC'];
        if (pulse_v) {
            let pulse_v_children = pulse_v.getElementsByTagName("div");
            const size = pulse_v_children.length;
            for (let i = 0; i < size; i++) {
                let size_div = (200 + (size - i) * 20) / 1.618;
                window.setTimeout(() => {
                    let pulse_div = pulse_v_children[i];
                    pulse_div.style.width = `${size_div}px`;
                    pulse_div.style.height = `${size_div}px`;
                    size_div = (200 + (size - i) * 20) / 1.618;
                    pulse_v_children[i].style.border = `2px solid ${colors[i]}`;
                    pulse_v_children[i].style.backgroundColor = `${colors[i]}`;
                    i = (i + 1) % size;
                }, i * 500);
            }
        }
    }

    /**
     * elle permet la mise en place de processing bar avec/sans animations
     * selon les classes utilisées
     * @param {*} selector_p 
     */
    process(selector_p) {
        let process_bar_v = document.querySelectorAll(selector_p);

        if (process_bar_v) {

            process_bar_v.forEach(process => {
                const processed_element_v = process.firstElementChild;
                //animation locale => propre à javascript
                const animate = (width) => {
                    let countor = 0;
                    //animation avec apparition de comptage de pourcentage
                    if (processed_element_v.classList.contains("level")) {
                        window.setInterval(() => {
                            if (countor < width) {
                                countor = countor + 1;
                                processed_element_v.textContent = `${countor}%`;
                                processed_element_v.style.width = `${countor}%`;
                            }
                        }, 100);
                    } else {
                        //animation sans le comptage de pourcentage
                        window.setInterval(() => {
                            if (countor < width) {
                                countor = countor + 1;
                                processed_element_v.style.width = `${countor}%`;
                            }
                        }, 100);
                    }

                }

                if (processed_element_v) {
                    let width = parseInt(processed_element_v.dataset["processed"]);
                    if (processed_element_v.classList.contains("animate")) {
                        animate(width);
                    } else {
                        processed_element_v.style.width = `${width}%`;
                    }
                }
            });
        }
    }
    /**
     * elle met en place le carousel y compris les animations des images et puces ou slides
     * sous images
     * @param {*} selector 
     */
    carousel(selector_p) {
        const carousel_v = document.querySelector(selector_p);
        //Group des images ou  container des images
        let carousel_group = "";

        //Toutes les images
        let carousel_images_list = [];

        if (carousel_v) {
            carousel_group = carousel_v.querySelector(`${selector_p}-group`);
            if (carousel_group) {

                carousel_images_list = carousel_group.querySelectorAll(".carousel-group-item");
                let carousel_slider = [];

                //Append bubble slider for images
                const slider_bubble = document.querySelector(".carousel-group-bubble");

                //Carousel animate action
                const carousel_animate = (current_index) => {
                    console.log(current_index)
                    //current image element
                    let carousel_image_current = carousel_images_list[this.carousel_iterator];
                    //current iterator
                    let old_carousel_iterator = this.carousel_iterator;
                    //new iterator
                    this.carousel_iterator = current_index;
                    //new image element to show
                    let carousel_image_next = carousel_images_list[current_index];
                    if (carousel_image_current && carousel_image_current.classList.contains("show")) {
                        carousel_image_current.classList.remove("show");
                        carousel_image_next.classList.add("show");

                        //Bubble slider under image
                        carousel_slider[current_index].classList.add("show");
                        carousel_slider[old_carousel_iterator].classList.remove("show");
                    }
                }
                if (slider_bubble) {
                    //Creation de boule de slider qui se trouvent sur l'image
                    for (let i = 0; i < carousel_images_list.length; i++) {
                        const div_element = document.createElement("div");
                        div_element.id = i + 1;
                        div_element.classList.add("carousel-group-bubble-item");
                        carousel_slider.push(div_element);
                        slider_bubble.appendChild(div_element);

                        //click event obn bubble element under image
                        div_element.addEventListener("click", e => {
                            let temp_carousel_iterator = (parseInt(e.currentTarget.id) - 1) % carousel_images_list.length;
                            carousel_animate(temp_carousel_iterator);
                        })
                    }
                    //for the first image bubble
                    carousel_slider[this.carousel_iterator].classList.add("show");
                }
                //Animation
                window.setInterval(() => {
                    let temp_carousel_iterator = (this.carousel_iterator + 1) % carousel_images_list.length;
                    //animate
                    carousel_animate(temp_carousel_iterator);
                }, 5000);
            }
        }

    }

    //TODO méthode accordeon pas encore finie
    accordeon(selector) {
        const accord_title = document.querySelectorAll(selector);
        if (accord_title) {
            accord_title.forEach(accord_title_item => {
                accord_title_item.addEventListener("click", e => {
                    if (e.currentTarget && e.currentTarget.parentElement) {
                        if (e.currentTarget.parentElement.classList.contains("active")) {
                            e.currentTarget.parentElement.classList.add("active");
                            const content_element = e.currentTarget.parentElement.getElementsByClassName("accordeon-title-content");
                            if (content_element[0]) {
                                content_element[0].textContent = "";
                            }
                            e.currentTarget.parentElement.classList.remove("active");
                        } else {
                            const active_element = document.querySelector(".active");
                            if (active_element) {
                                active_element.classList.remove("active");
                            }
                            e.currentTarget.parentElement.classList.add("active");
                            const content_element = e.currentTarget.parentElement.getElementsByClassName("accordeon-title-content");
                            setTimeout(() => {
                                if (content_element[0]) {
                                    content_element[0].textContent = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum obcaecati velit porro non voluptates, `;
                                }
                            }, 200);
                        }
                    }
                })
            })
        }
    }

    slider(selector) {
        const container = document.querySelector(selector);

        if (container) {

            let isDown = false;
            let start_position_x;
            let container_Scroll_offSetLeft;

            container.addEventListener("mousedown", (e) => {
                isDown = true;
                container.classList.add("active");
                start_position_x = e.pageX - container.offsetLeft;
                container_Scroll_offSetLeft = container.scrollLeft;
            });

            container.addEventListener("mouseup", (e) => {
                isDown = false;
                container.classList.remove("active");
            });

            container.addEventListener("mouseleave", (e) => {
                container.classList.remove("active");
                isDown = false;
            });

            container.addEventListener("mousemove", (e) => {
                if (!isDown) return;

                e.preventDefault();
                const x = e.pageX - container.offsetLeft;
                const avance = x - start_position_x;
                const final_value = container_Scroll_offSetLeft - avance;
                container.scrollLeft = final_value;
            });
        }
    }

    /**
     * @brief  ce procedure permet lors d'un clique/focus sur 
     * un input/champ
     * qu'il apparait un label qui sorte du lieu où se trouve
     *  le curseur vers sa place
     * idéalle.
     */
    form_input_animate() {
        const form_groups = document.getElementsByClassName("form-group");
        var form_group_array = [];
        if (form_groups) {
            //Selection de forms ayant en plus une classe "animation"
            for (var i = 0; i < form_groups.length; i++)
                if (form_groups[i].classList.contains("animate"))
                    form_group_array.push(form_groups[i]);
        }
        //Mise en place l"événement pour chaque champs input
        form_group_array.forEach(form => {
            const input_field = form.getElementsByTagName("input");
            const label_field = form.getElementsByTagName("label");
            if (input_field.length != 0 && label_field.length != 0 &&
                input_field[0] && label_field[0]) {
                input_field[0].addEventListener("focus", e => {
                    label_field[0].classList.add("focus");
                    input_field[0].addEventListener("focusout", e => {
                        if (label_field[0].classList.contains("focus") &&
                            input_field[0].value.length === 0) {
                            label_field[0].classList.remove("focus");
                            console.log(input_field[0].value)
                        }
                    });
                });
            }
        });
    }

    /**
     * elle permet lors qu'on clicke sur
     * la croise en droite d'une carte
     * d'afficher le contenu caché dernier
     * Puis, clique sur la ceoix de le cacher
     */
    card_show_back_items() {
        const items = document.querySelectorAll(".card-right-top-show");
        items.forEach(item => {

            item.addEventListener("click", e => {
                const element = e.currentTarget.parentElement.offsetParent;
                const elem_to_close = element.getElementsByClassName("card-right-back")[0];
                const btn_close_elem = elem_to_close.getElementsByClassName("card-right-top-close")[0];

                if (elem_to_close) {
                    //Toggle
                    elem_to_close.classList.add("active");
                    btn_close_elem.addEventListener("click", e => {
                        elem_to_close.classList.remove("active");
                    })
                }

            });

        });
    }

    linkEvent() {
        const all_links = document.querySelectorAll("li a");
        /*Check parant container UL */
        if (document.querySelector('li') &&
            document.querySelector('li').parentElement.nodeName === 'UL') {
            all_links.forEach(link => {
                link.addEventListener("click", e => {
                    const ULelement = link.parentElement.parentElement
                    var oldActiveElement = ULelement.getElementsByClassName('active');
                    var active_array_element = [].slice.call(oldActiveElement);
                    let setActive = true;
                    //block change from a page to other
                    // e.preventDefault();
                    active_array_element.map(elem => {
                        if (!e.currentTarget.parentElement.classList.contains('desable')) {
                            elem.classList.remove("active");
                        } else {
                            //is link desabled
                            setActive = false
                        }
                    });

                    //Set active to active link
                    if (!link.classList.contains("active") && setActive) {
                        link.parentElement.classList.add("active");
                    }
                })
            })
        }
    }

    dropdown() {

    }

    alert() {
        const alert_close_btn = document.querySelectorAll(".alert-close");
        alert_close_btn.forEach(close_btn => {
            close_btn.textContent = "x";
            close_btn.addEventListener('click', e => {
                if (e.currentTarget.parentElement) {
                    e.currentTarget.parentElement.remove();
                }
            })
        })
    }


    /*ASIDE != ASIDE PROFILE*/
    aside(selector_container) {
        const aside = document.querySelector(selector_container);
        const aside_show_btn = document.querySelector(".aside-show");
        if (aside_show_btn) {
            aside_show_btn.addEventListener('click', e => {
                const list_element = aside.getElementsByClassName("aside-hide");
                const size = list_element.length;
                e.preventDefault();
                /*CHANGE CHEVRON BTN*/
                if (aside_show_btn.firstElementChild && aside_show_btn.firstElementChild.classList.contains('fa-chevron-double-right')) {
                    aside_show_btn.firstElementChild.classList.remove('fa-chevron-double-right');
                    aside_show_btn.firstElementChild.classList.add('fa-chevron-double-left');
                } else {
                    aside_show_btn.firstElementChild.classList.remove('fa-chevron-double-left');
                    aside_show_btn.firstElementChild.classList.add('fa-chevron-double-right');
                }
                for (let index = 0; index < list_element.length; index++) {

                    if (list_element[index].classList.contains('aside-hide-active')) {

                        setTimeout(() => {
                            list_element[index].classList.remove('aside-hide-active');

                        }, index * 100);
                    } else {

                        setTimeout(() => {
                            list_element[index].classList.add('aside-hide-active');
                        }, index * 100);
                    }
                }
            })
        }
    }

    aside_profile_activate(selector_p, scroll_value = 0) {

        //on fera apparaitre que le bouton go_up
        const aside_profile_v = document.querySelector(selector_p);
        if (document.documentElement.scrollTop > scroll_value) {
            //Activation d'Aside Profile Bar en vérifiant si user ne l'a pas masqué
            if (aside_profile_v && this.aside_profile_checker) {
                aside_profile_v.classList.add("active");
            }
        } else {
            if (aside_profile_v) {
                aside_profile_v.classList.remove("active");
            }
        }
    }

    /*ASIDE - PROFILE CLOSE*/
    /**
     * elle permet de cacher le aside profile
     * Event
     * @param {*} selector 
     */
    aside_profile_close(selector) {

        const close_element = document.querySelector(selector);
        if (close_element) {
            close_element.addEventListener("click", e => {
                e.preventDefault();
                const aside_profile = document.querySelector(".aside-profile-container");
                if (aside_profile) {
                    if (aside_profile.classList.contains("active")) {
                        aside_profile.classList.remove("active");
                        this.aside_profile_checker = false;
                    } else {
                        aside_profile.classList.add("active");
                        this.aside_profile_checker = true;
                    }
                }
            });
        }
    }

    // GENERAL ------------------- SCROLL
    activateScroll(selector) {
        window.onscroll = () => {
            const go_up = document.querySelector(selector);
            this.scrollUp(go_up, this.cv_nav_bar);
            this.aside_profile_activate(".js-main-home-left");
        }
    }

    activate_go_up(selector) {
        $(selector).on('click', e => {
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, '300');
        });
    }

    /**
     * Elle affiche le boutton scroll Up lors
     * qu'on scrolle de 30%
     */
    scrollUp(btn_go_up_elem, nav_list_item_elem) {
        //TODO refactoring -
        //Pour la page CV- Affiche le bouton et rend fixe la bare de naavigation 
        if (btn_go_up_elem && nav_list_item_elem) {
            if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 450) {
                btn_go_up_elem.style.display = "block";
                /*Rendre la séconde barre de navégation de CV fixe*/
                nav_list_item_elem.classList.add("navbar-fixed");
            } else {
                //On retire la classe fixed
                nav_list_item_elem.classList.remove("navbar-fixed");
                btn_go_up_elem.style.display = "none";
            }
        } else if (btn_go_up_elem) { //Cas où navbar n'existe pas
            //on fera apparaitre que le bouton go_up
            const aside = document.querySelector(".main-home-left");
            if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 450) {
                btn_go_up_elem.style.display = "block";
                //Activation d'Aside Profile Bar
                if (aside && this.aside_profile_checker) {
                    aside.classList.add("active");
                }
            } else {
                btn_go_up_elem.style.display = "none";
                if (aside)
                    aside.classList.remove("active");
            }
        }
    }

    /**
     * Listener for doing scroll effect
     * @Brief pour chaque element ayant la classe (.scroll-animate) 
     * et un identifiant de la forme (identifiant). Lors d'un événnement clique.
     * On vérifie bien l'existance de ces deux puis on appelle la fonction scroll avec
     * identifiant bien formé.
     * @Usage exemple: <tag class="scroll-animate" id="portfolio">Clique here!</tag>
     * Et quelque part dans le fichier doit y avoir un élèment ayant 
     * un identifiant de forme (identifiant-index)
     * @refactoring : Faire en sort que cette fonction prenne 
     * en compte le e.currentTaget aulieu de e.target.par....
     */
    activate_link_scroll() {
        this.scroll_animation.on("click", (e) => {
            //TODO Rectifier
            if ((e.target.parentElement && e.target.parentElement.id)) {
                //des constantes pour titre de compréhension.
                //sinon pas besoin
                const destination = `#${e.target.parentElement.id}-index`;
                this.scroll(destination);
            } else if ((e.currentTarget && e.currentTarget.id)) {
                const destination = `#${e.currentTarget.id}-index`;
                this.scroll(destination);
            }
            e.preventDefault();
        });
    }

    /**
     * @Require jQuery minify
     * @param selectorLinkTo
     * @brief Le scroll se réalise grâce au cible(id,class, etc) 
     * passé en argument. Ce cible est atteint par sa position.
     */
    scroll(selectorLinkTo) {
        if (document.querySelector(selectorLinkTo)) {
            const element = $(selectorLinkTo);
            const images = element.position().top;
            //Faire de l'animation
            const delay = 500;
            $('html, body').animate({
                scrollTop: images
            }, delay);
        }
    }

    //Vérification de l'existance de formule (source d'erreur)
    formular_checker(selector) {
        const formular = document.querySelector(selector);

        if (formular) {
            formular.addEventListener("submit", e => {
                e.preventDefault();
                //recolte de valeurs de champs
                const name = document.querySelector('#name');
                const email = document.querySelector('#email');
                const subject = document.querySelector('#subject');
                const message = document.querySelector('#message');
                const messageArea = document.querySelector("#messageArea");

                console.log(email)
                if (name.value !== "" &&
                    email.value !== "" &&
                    subject.value !== "" &&
                    message.value !== "") {

                    name.value = "";
                    email.value = "";
                    subject.value = "";
                    message.value = "";

                    messageArea.classList.add("alert");
                    messageArea.classList.add("success");
                    messageArea.textContent = 'Votre message est envoyé avec succès!';


                } else {
                    messageArea.classList.add("alert");
                    messageArea.classList.add("danger");
                    messageArea.textContent = 'Veuillez remplir tous les champs';
                }
                setTimeout(() => {
                    messageArea.textContent = "";
                    messageArea.classList = [];
                }, 3000);
            });
        }
    }
}
/* LINK - ACTIVE */

//To active
var tft = new TFT();
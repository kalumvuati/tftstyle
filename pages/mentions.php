<section class="mentions">

   <div class="container">

    <div class="mentions-group">

        <!--LEGAL INFOS-->
        <?php if  (!empty($url) && !empty($site_title)&& $legale) : ?>
            <div class="mentions-group-legale">
                <h1>Mentions legales</h1>
                <p>
                    Conformément aux dispositions des articles 6-III et 19 de la Loi n° 2004-575 du 21 juin 2004 pour la Confiance dans l'économie numérique, 
                    dite L.C.E.N., nous portons à la connaissance des utilisateurs et visiteurs du site 
                    : <a href="?p=home"><?= $url ?></a> - <?= $site_title?> les informations suivantes :
                </p>
            </div>
        <?php endif; ?>
        <!--LEGAL NOTICE-->
        <?php if  (!empty($url) && !empty($site_title)&& $infos) : ?>
            <div class="mentions-group-infos">
                <h2 class="mention-title">Informations légales</h2>

                <p>Statut du propriétaire : particulier</p>
                <p>
                    Propriétaire, créateur, responsable de publication, webmaster : KALUMVUATI Duramana - 67100 Strasbourg - duramana.kalumvuati@laposte.net.
                    Le responsable de la publication est une personne physique.
                </p>
                <p>Hébergeur :1&1 IONOS SARL, 7 place de la Gare, BP 70109, 57201 Sarreguemines Cedex.</p>
            </div>
        <?php endif; ?>
        <!--ACCESSIBILITY-->
        <?php if  (!empty($url) && !empty($site_title)&& $accessibilite) : ?>
            <div class="mentions-group-accessibilite">
                <h2 class="mention-title">Accessibilité</h2>
                <p>
                    Le site <a href="?p=home"><?= $url ?></a>- <?= $site_title?> est par principe accessible aux utilisateurs 24/24h, 7/7j, 
                    sauf interruption, programmée ou non, pour les besoins de sa maintenance ou en cas de force majeure. 
                    En cas d’impossibilité d’accès au service, le webmaster s’engage à faire son maximum afin de rétablir l’accès au service.
                </p>
            </div>
        <?php endif; ?>
        <!--INTELIGENT PROPERTY-->
        <?php if  (!empty($url) && !empty($site_title)&& $intelect) : ?>
            <div class="mentions-group-intellectuelle">
                <h2 class="mention-title">Propriété intellectuelle</h2>
                <p>
                    KALUMVUATI Duramana est propriétaire exclusif de tous les droits de propriété intellectuelle ou détient les droits d’usage sur tous les éléments accessibles 
                    sur le site <a href="?p=home"><?= $url ?></a>- <?= $site_title?>, tant sur la structure que sur les textes, images, graphismes, logo, icônes, sons, logiciels…
                    Toute reproduction totale ou partielle du site http://www.kaldur.dipsw-ccicampus.fr, représentation, modification, publication, adaptation totale ou partielle de l'un de ces éléments, 
                    quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de KALUMVUATI Duramana, propriétaire du site à l'e-mail : duramana.kalumvuati@laposte.net 
                    à défaut elle sera considérée comme constitutive d’une contrefaçon et passible de poursuite conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.
                </p>
            </div>
        <?php endif; ?>
        
        <!--COOKIES-->
        <?php if  (!empty($url) && !empty($site_title) && $cookies) : ?>
            <div class="mentions-group-cookies">
                <h2 class="mention-title">Liens hypertextes et cookies</h2>
                <p>
                    Le site <a href="?p=home"><?=$url?></a>- <?= $site_title ?> contient un certain nombre de liens hypertextes vers d’autres sites (partenaires, informations) 
                    mis en place avec l’autorisation de KALUMVUATI Duramana. Cependant, KALUMVUATI Duramana n’a pas la possibilité de vérifier l'ensemble du contenu des sites ainsi visités et décline donc toute responsabilité de ce fait quand aux risques éventuels de contenus illicites.
                    L’utilisateur est informé que lors de ses visites sur le site <a href="?p=home"><?=  $url?></a>- <?=$site_title ?>, un ou des cookies sont susceptibles de s’installer automatiquement sur son ordinateur par l'intermédiaire de son navigateur. 
                    Un cookie est un bloc de données qui ne permet pas d'identifier l'utilisateur, mais qui enregistre des informations relatives à la navigation de celui-ci sur le site.
                    Le paramétrage du navigateur permet d’informer de la présence de cookie et éventuellement, de la refuser de la manière décrite à l’adresse suivante : www.cnil.fr. 
                    L’utilisateur peut toutefois configurer le navigateur de son ordinateur pour refuser l’installation des cookies, sachant que le refus d'installation d'un cookie peut entraîner l’impossibilité d’accéder à certain service.
                </p>
            </div>
        <?php endif; ?>

    </div>
   </div>
    
</section>
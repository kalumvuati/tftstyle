<footer class="cv-footer">
    <div class="cv-footer-social">
        <ul class="cv-footer-social-group">

            <li class="cv-footer-social-group-item" >
                <a href="https://www.linkedin.com/in/duramana-kalumvuati-44026715b/">
                    <i class="fab fa-linkedin-in"></i>
                    <i class="fab fa-linkedin-in"></i>
                </a>
            </li>

            <li class="cv-footer-social-group-item">
                <a href="https://git.unistra.fr/kalumvuati">
                <i class="fab fa-gitlab"></i>
                <i class="fab fa-gitlab"></i>
            </a>
            </li>
            <li class="cv-footer-social-group-item">
                <a href="https://www.youtube.com/channel/UC9Fe1h1C23IshOPDU7uFH6Q?view_as=subscriber">
                    <i class="fab fa-youtube"></i>
                    <i class="fab fa-youtube"></i>
                </a>
            </li>
        </ul>
    </div>

    <div>
    </div>
    <p>&copy;copyright <?=$this_year->format('Y')?> <?=$name?> - <a href="?p=mentions">mentions légales</a></p>
    
</footer>